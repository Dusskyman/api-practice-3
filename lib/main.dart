import 'package:api_practice_3/data_repository.dart';
import 'package:api_practice_3/product_card.dart';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String find = 'colourpop';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50, left: 10, right: 10),
              child: TextFormField(
                onFieldSubmitted: (value) {
                  setState(
                    () {
                      find = value;
                    },
                  );
                },
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.8,
              child: FutureBuilder(
                future: DataRepository.getProduct(find),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) => ProductCard(
                        snapshot.data[index],
                      ),
                    );
                  } else if (snapshot.connectionState == ConnectionState.none) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Center(
                      child: Text(
                        'Enter your brand',
                        style: TextStyle(fontSize: 40),
                      ),
                    );
                  } else
                    return Center(
                      child: Text(
                        'Not Found :(',
                        style: TextStyle(fontSize: 40),
                      ),
                    );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
