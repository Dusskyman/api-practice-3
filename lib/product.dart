class Product {
  final id;
  final String brand;
  final String name;
  final String price;
  final String imageUrl;
  final String productType;
  final String description;
  final double rating;

  Product.buildFromMap(Map map)
      : id = map['id'],
        brand = map['brand'],
        name = map['name'],
        price = map['price'],
        description = map['description'],
        imageUrl = map['image_link'],
        productType = map['product_type'],
        rating = map['rating'];
}
