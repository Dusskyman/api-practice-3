import 'package:api_practice_3/product.dart';
import 'package:flutter/material.dart';

class DetailProduct extends StatelessWidget {
  final Product product;

  const DetailProduct(this.product);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              fit: StackFit.loose,
              children: [
                Hero(
                  tag: '${product.id}',
                  child: Image.network(
                    product.imageUrl,
                    errorBuilder: (context, error, stackTrace) => Image.network(
                        'https://image.freepik.com/free-vector/glitch-error-404-page_23-2148105404.jpg'),
                  ),
                ),
                Positioned(
                  top: 10,
                  left: 0,
                  child: SizedBox(
                    width: 150,
                    child: Text(
                      product.name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(color: Colors.grey, fontSize: 30),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  right: 10,
                  child: SizedBox(
                    width: 50,
                    child: product.rating != 0 && product.rating != null
                        ? Row(
                            children: List.generate(
                              product.rating.toInt(),
                              (index) => Icon(
                                Icons.star,
                                size: 10,
                              ),
                            ),
                          )
                        : SizedBox(),
                  ),
                ),
                Positioned(
                  left: 10,
                  bottom: 10,
                  child: Text(
                    '${product.price}\$',
                    style: TextStyle(color: Colors.grey, fontSize: 30),
                  ),
                ),
              ],
            ),
            Text(
              'Description',
              style: TextStyle(fontSize: 30),
            ),
            Text(product.description),
          ],
        ),
      ),
    );
  }
}
