import 'dart:convert';

import 'package:api_practice_3/product.dart';
import 'package:http/http.dart' as http;

class DataRepository {
  static Future<List> getProduct(String value) async {
    return await http
        .get(
            'http://makeup-api.herokuapp.com/api/v1/products.json?brand=$value')
        .then((value) {
      return (jsonDecode(value.body) as List)
          .map((e) => Product.buildFromMap(e))
          .toList();
    });
  }
}
